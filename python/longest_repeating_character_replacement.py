from typing import List
import unittest


class RepeatingCharLen:
    def __init__(self, char, k):
        """Helper structure to compute the longest length of repeating
            characters."""
        self.char = char
        self.length = 0
        self.k = k
        self.diff_indexes = []

    def pretty_print(self):
        """Pretty please, print…?"""
        print("\'{}\':\n\tk: {}\n\tLength: {}\n\tDiff_indexes: {}".format(
            self.char, self.k, self.length, self.diff_indexes
        ))


class Solution:

    def character_replacement(self, s: str, k: int) -> int:
        """Given a string `s` that consists of only uppercase English letters,
        you can perform at most `k` operations on that string. In one
        operation, you can choose any character of the string and change it to
        any other uppercase English character.

        Find the length of the longest sub-string containing all repeating
        letters you can get after performing the above operations.

        Runtime: faster than 5.02% of answers.
        Memory Usage: less than 5.15% of answers.
        """
        len_max = 0
        chars = {}

        for char in s:
            if char not in chars:
                chars[char] = RepeatingCharLen(char, k)

        for (index, char) in enumerate(s):
            for rep_char in chars.values():
                if rep_char.char != char:
                    if rep_char.k == 0:
                        len_max = max(len_max, rep_char.length)
                        try:
                            rep_char.length = \
                                index - rep_char.diff_indexes.pop(0)
                        except IndexError:
                            rep_char.length = 0
                    else:
                        rep_char.k -= 1
                        rep_char.length += 1

                    rep_char.diff_indexes.append(index)

                else:
                    rep_char.length += 1

                rep_char.pretty_print()

        return max(len_max, max([rep_char.length for rep_char in
                                 chars.values()]))

    def character_replacement_2(self, s: str, k: int) -> int:
        """Solution I copied and I'm trying to understand."""
        if s == "":
            return 0

        counts = {}
        start = 0
        most_freq = s[0]

        for (i, char) in enumerate(s):
            if char not in counts:
                counts[char] = 0

            counts[char] += 1

            if counts[char] > counts[most_freq]:
                most_freq = char

            if i - start + 1 - counts[most_freq] > k:
                counts[s[start]] -= 1
                start += 1

            print("-----")
            print("i: {}, char: {}".format(i, char))
            print("counts:\n\t{}".format(counts))
            print("start: {}".format(start))
            print("most_freq: {}".format(most_freq))

        return len(s) - start


class TestSolution(unittest.TestCase):
    def test_character_replacement(self):
        s = Solution()

        # self.assertEqual(s.character_replacement("ABAB", 2), 4)
        # self.assertEqual(s.character_replacement("AABABBA", 1), 4)
        self.assertEqual(s.character_replacement_2("ABABAABAABBBB", 2), 8)
        # self.assertEqual(s.character_replacement("ABABAABAABBBB", 1), 5)
        # self.assertEqual(s.character_replacement("ABCDE", 1), 2)
        # self.assertEqual(s.character_replacement("AAAB", 0), 3)
