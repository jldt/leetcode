import unittest
from typing import List


class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        """Give an collection of intervals, merge all overlapping intervals.

        EVALUATION:
        - Runtime: faster than 42.43%.
        - Memory Usage: less than 6.52%."""
        if len(intervals) == 0:
            return []

        intervals.sort(key=lambda x: x[0])

        res = []
        prev = intervals[0]
        for i in range(1, len(intervals)):
            curr = intervals[i]
            if prev[1] >= curr[0]:
                if prev[1] < curr[1]:
                    prev[1] = curr[1]
            else:
                res.append(prev)
                prev = curr

        res.append(prev)
        return res


class TestSolution(unittest.TestCase):
    def test_merge(self):
        s = Solution()

        self.assertEqual(s.merge([[1, 3], [2, 6], [8, 10], [15, 18]]),
                         [[1, 6], [8, 10], [15, 18]])
        self.assertEqual(s.merge([[1, 4], [4, 5]]), [[1, 5]])
        self.assertEqual(s.merge([[1, 8], [2, 6], [1, 2]]), [[1, 8]])
