from typing import List
import unittest


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        """Given an array `nums` of integers, are there elements `a`, `b`, `c`
        in `nums` such that `a + b + c = 0`? Find all unique triplets in the
        array which gives the sum of zero.

        The solution set must not contain duplicate triplets.

        EVALUATION:
        - Runtime: 1740ms, faster than 22.26%.
        - Memory Usage: 17.3MB, less than 18.57%."""
        nums.sort()

        res = set()
        for i in range(len(nums) - 2):
            j = i + 1
            k = len(nums) - 1

            while j < k:
                tot = nums[i] + nums[j] + nums[k]
                if tot == 0:
                    res.add((nums[i], nums[j], nums[k]))
                    j += 1
                    k -= 1
                elif tot < 0:
                    j += 1
                else:
                    k -= 1

        return [list(triplet) for triplet in res]

    def threeSumTimeLimitExceeded3(self, nums: List[int]) -> List[List[int]]:
        """Given an array `nums` of integers, are there elements `a`, `b`, `c`
        in `nums` such that `a + b + c = 0`? Find all unique triplets in the
        array which gives the sum of zero.

        The solution set must not contain duplicate triplets.

        EVALUATION: The timeout was reached after 311/313 tests."""
        possible_sums = {}
        dict_nums = {}
        for i, n in enumerate(nums):
            if n not in dict_nums:
                dict_nums[n] = 0

            dict_nums[n] += 1
            if dict_nums[n] > 1:
                continue

            for m in nums[i+1:]:
                if (n, m) not in possible_sums and (m, n) not in possible_sums:
                    possible_sums[(n, m)] = False

        res = []
        for (n, m), isDuplicate in possible_sums.items():
            if isDuplicate:
                continue

            compl = - (n + m)
            if compl in dict_nums:
                prev_n = dict_nums[n]
                prev_m = dict_nums[m]

                dict_nums[n] -= 1
                dict_nums[m] -= 1

                if dict_nums[compl] > 0:
                    possible_dupl = [(n, compl), (compl, n),
                                     (m, compl), (compl, m)]
                    for dupl in possible_dupl:
                        if dupl in possible_sums:
                            possible_sums[dupl] = True

                    res.append([n, m, compl])

                dict_nums[n] = prev_n
                dict_nums[m] = prev_m

        return res

    def threeSumTimeLimitExceeded(self, nums: List[int]) -> List[List[int]]:
        """Given an array `nums` of integers, are there elements `a`, `b`, `c`
        in `nums` such that `a + b + c = 0`? Find all unique triplets in the
        array which gives the sum of zero.

        The solution set must not contain duplicate triplets.

        EVALUATION: The timeout was reached after 311/313 tests."""
        dict_nums = {}
        sums_2 = {}

        for i, n in enumerate(nums):
            if n not in dict_nums:
                dict_nums[n] = 1
            else:
                dict_nums[n] += 1

            for m in nums[i+1:]:
                if (n, m) not in sums_2 and (m, n) not in sums_2:
                    sums_2[(n, m)] = False

        res = []
        for (n, m), duplicate in sums_2.items():
            if duplicate:
                continue

            complementary = - (n + m)
            if complementary in dict_nums:
                prev_n = dict_nums[n]
                prev_m = dict_nums[m]

                dict_nums[n] -= 1
                dict_nums[m] -= 1

                if dict_nums[complementary] > 0:
                    other_dupl = [(n, complementary), (complementary, n),
                                  (m, complementary), (complementary, m)]
                    for dupl in other_dupl:
                        if dupl in sums_2:
                            sums_2[dupl] = True

                    res.append([n, m, complementary])

                dict_nums[n] = prev_n
                dict_nums[m] = prev_m

        return res

    def findIndexFirstPositive(self, nums: List[int]) -> int:
        """Find the index of the first positive integer."""
        start = 0
        end = len(nums)

        while True:
            middle = int((start + end) / 2)
            if nums[middle] >= 0 and nums[middle - 1] < 0:
                return middle
            if nums[middle] >= 0 and nums[middle - 1] >= 0:
                end = middle
            else:
                start = middle

    def threeSumTimeLimitExceeded2(self, nums: List[int]) -> List[List[int]]:
        """Given an array `nums` of integers, are there elements `a`, `b`, `c`
        in `nums` such that `a + b + c = 0`? Find all unique triplets in the
        array which gives the sum of zero.

        The solution set must not contain duplicate triplets.

        EVALUATION: The timeout was reached after 36/313 tests."""
        if len(nums) < 3:
            return []

        nums.sort()
        index_positive = self.findIndexFirstPositive(nums)

        negative_singles = set(nums[:index_positive])
        negative_sums = set()
        for i, n in enumerate(nums[:index_positive]):
            for m in nums[i+1:index_positive]:
                negative_sums.add((n, m))

        positive_singles = set(nums[index_positive:])
        positive_sums = set()
        for i in range(index_positive, len(nums)):
            n = nums[i]
            for m in nums[i+1:]:
                positive_sums.add((n, m))

        res = []
        for (n, m) in negative_sums:
            complement = - (n + m)
            if complement in positive_singles:
                res.append([n, m, complement])

        for (n, m) in positive_sums:
            complement = - (n + m)
            if complement in negative_singles:
                res.append([n, m, complement])

        if (
                nums[index_positive] == 0 and
                index_positive + 2 < len(nums) and
                nums[index_positive + 2] == 0
        ):
            res.append([0, 0, 0])

        return res


class TestSolution(unittest.TestCase):
    def test_threeSum(self):
        s = Solution()

        self.assertEqual(s.threeSum([-1, 0, 1, 2, -1, -4]),
                         [[-1, 0, 1], [-1, -1, 2]])
