from typing import List
import unittest


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        """Given an array of integer `nums`, return indices of the two numbers
        such that they add up to `target`.

        We assume that each input has exactly one answer and the same element
        cannot be reused twice.
        """
        for i in range(0, len(nums)):
            for j in range(i+1, len(nums)):
                if nums[i] + nums[j] == target:
                    return [i, j]

    def twoSumHash(self, nums: List[int], target: int) -> List[int]:
        """"""
        h = {}
        for i, num in enumerate(nums):
            n = target - num
            if n not in h:
                h[num] = i
            else:
                return [h[n], i]


class TestSolution(unittest.TestCase):

    def test_twoSum(self):
        s = Solution()

        self.assertEqual(s.twoSum([1, 2, 3, 4], 7), [2, 3])
        self.assertEqual(s.twoSum([1, 2, 3, 4], 3), [0, 1])
        self.assertEqual(s.twoSum([1, 2, 3, 4], 6), [1, 3])
        self.assertEqual(s.twoSum([1, 2, 3, 4], 4), [0, 2])

    def test_twoSumHash(self):
        s = Solution()

        self.assertEqual(s.twoSumHash([1, 2, 3, 4], 7), [2, 3])
        self.assertEqual(s.twoSumHash([1, 2, 3, 4], 3), [0, 1])
        self.assertEqual(s.twoSumHash([1, 2, 3, 4], 6), [1, 3])
        self.assertEqual(s.twoSumHash([1, 2, 3, 4], 4), [0, 2])


if __name__ == '__main__':
    unittest.main()
