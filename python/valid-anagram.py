import unittest


class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        """Given two strings `s` and `t`, write a function to determine if `t`
        is an anagram of `s`.

        You may assume the string contains only lowercase alphabets."""
        if len(s) != len(t):
            return False

        letters = {}
        for letter in s:
            if letter in letters:
                letters[letter] += 1
            else:
                letters[letter] = 1

        for letter in t:
            if letter not in letters:
                return False
            else:
                letters[letter] -= 1
                if letters[letter] == 0:
                    del letters[letter]

        return True


class TestSolution(unittest.TestCase):
    def test_isAnagram(self):
        s = Solution()
        self.assertTrue(s.isAnagram("anagram", "nagaram"))
        self.assertFalse(s.isAnagram("rat", "car"))
        self.assertFalse(s.isAnagram("raat", "carr"))
