import unittest
from typing import List


class Solution:
    def max_area(self, height: List[int]) -> int:
        """Given `n` non-negative integers `a_1, a_2, ..., a_n`, where each
        represents a point at coordinate `(i, a_i`), `n` vertical lines are
        drawn such that the two endpoints of line `i` is at `(i,a_i)` and `(i,
        0)`. Find two lines, which together with the x-axis form a container,
        such that the container contains the most water."""

        # Brute force solution.
        # max_area = 0
        # for (index, elt) in enumerate(height):
        #     for (subindex, other) in enumerate(height[index:]):
        #         area = min(elt, other) * (subindex - index + 1)
        #         max_area = max(max_area, area)

        # return max_area

        # Double-pointer solution: we start at the beginning and the end of the
        # array (maximum distance) and go towards the center only moving the
        # index of the lowest height between the head and the tail.
        max_area = 0
        head = 0
        tail = len(height) - 1

        while head != tail:
            max_area = max(max_area,
                           min(height[head], height[tail]) * (tail - head))

            if height[head] >= height[tail]:
                tail -= 1
            else:
                head += 1

        return max_area
