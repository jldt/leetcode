"""Exercise from the Leetcode website."""

import unittest
from typing import List


class Solution:
    """Solution to the exercise!"""

    pivot: int = -1

    def search(self, nums: List[int], target: int) -> int:
        """Suppose an array sorted in ascending order is rotated at some pivot
        unknown to you beforehand.

        You are given a target value to search. If found in the array return
        its index, otherwise return `-1`.

        You may assume no duplicates exists in the array.

        The runtime complexity must be in the order of O(log n).
        """
        self.pivot = -1
        return self.rec_search(nums, target)

    def rec_search(self, nums: List[int], target: int, offset: int = 0) -> int:
        """Recursive implementation"""
        print("\nCALL ----")
        print("\tnums: {}, target: {}, offset: {}, pivot: {}\n".format(
            nums, target, offset, self.pivot
        ))

        if not nums or (len(nums) == 1 and nums[0] != target):
            return -1

        ind_mid = len(nums) // 2
        mid = nums[ind_mid]
        print("ind_mid={}".format(ind_mid))

        curr_offset = ind_mid + 1
        print("curr_offset={}".format(curr_offset))

        if target == mid:
            print("TARGET found at: {} ({} + offset={})".format(
                ind_mid + offset, ind_mid, offset))
            return ind_mid + offset

        if self.pivot > -1:
            if target > mid:
                stop = len(nums)
                if (self.pivot - offset) >= ind_mid:
                    stop = self.pivot - offset

                print("55: nums[{}:{}]: {}".format(
                    ind_mid+1, stop, nums[ind_mid+1:stop])
                )

                return self.rec_search(nums[ind_mid+1:stop], target,
                                       offset + curr_offset)

            start = 0
            if (self.pivot - offset) <= ind_mid:
                start = self.pivot - offset

            print("66: nums[{}:{}]: {}".format(
                start, ind_mid, nums[start:ind_mid])
            )

            return self.rec_search(nums[start:ind_mid], target, offset)

        if ind_mid < (len(nums) - 1) and mid > nums[ind_mid + 1]:
            self.pivot = offset + ind_mid + 1
            print("PIVOT found at: {} ({} + offset={})".format(
                self.pivot, ind_mid + 1, offset
            ))
        elif ind_mid > 0 and mid < nums[ind_mid - 1]:
            self.pivot = offset + ind_mid - 1
            print("PIVOT found at: {} ({} + offset={})".format(
                self.pivot, ind_mid - 1, offset
            ))

        if target > mid:
            ind_right = self.rec_search(nums[ind_mid+1:len(nums)], target,
                                        offset + curr_offset)
            if ind_right > -1:
                return ind_right

            return self.rec_search(nums[0:ind_mid], target, offset)

        # else
        ind_left = self.rec_search(nums[0:ind_mid], target, offset)
        if ind_left > -1:
            return ind_left

        return self.rec_search(nums[ind_mid+1:len(nums)], target,
                               offset + curr_offset)


class TestSolution(unittest.TestCase):
    """Unit tests!"""

    def test_search(self):
        """Unit test for `solution_to_exercise` method."""
        sol = Solution()
        # self.assertEqual(sol.search([4, 5, 6, 7, 0, 1, 2], 0), 4)
        # self.assertEqual(sol.search([4, 5, 6, 7, 8, 0, 1, 2], 0), 5)
        # self.assertEqual(sol.search([4, 5, 6, 7, 0, 1, 2], 3), -1)
        # self.assertEqual(sol.search([8, 0, 1, 2, 4, 5, 6, 7], 8), 0)
        # self.assertEqual(sol.search([8, 0, 1, 2, 4, 5, 6, 7], 7), 7)
        # self.assertEqual(sol.search([8, 0, 1, 2, 4, 5, 6, 7], 0), 1)
        # self.assertEqual(sol.search([8, 0, 1, 2, 4, 5, 6, 7], 1), 2)
        # self.assertEqual(sol.search([4, 5, 6, 7, 8, 9, 1, 2, 3], 1), 6)
        self.assertEqual(sol.search([6, 7, 8, 0, 1, 3, 5], 1), 4)
