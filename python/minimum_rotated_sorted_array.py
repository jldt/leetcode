from typing import List
import unittest


class Solution:
    def minimum_rotated_sorted_array(self, nums: List[int]) -> int:
        """Suppose an array sorted in ascending order is rotated as some pivot
        unknown to you beforehand. Find the minimum element.

        You may assume no duplicate exists in the array."""
        head = 0
        tail = len(nums) - 1

        if len(nums) % 2 == 0:
            if nums[0] > nums[1]:
                return nums[1]

            head = 1

        while head != tail:
            if nums[head] > nums[head+1]:
                return nums[head+1]
            if nums[tail] < nums[tail-1]:
                return nums[tail]

            head += 1
            tail -= 1

        return nums[0]

    def find_min(self, nums: List[int]) -> int:
        """Suppose an array sorted in ascending order is rotated as some pivot
        unknown to you beforehand. Find the minimum element.

        You may assume no duplicate exists in the array."""

        # If the array is sorted, return the first element.
        if nums[0] <= nums[len(nums) - 1]:
            return nums[0]

        half = len(nums) // 2

        if nums[0] > nums[half]:
            # Remove the first element as it's greater than the element at half
            # and thus cannot be the minimum. Keep the element at half as it
            # could be the minimum.
            return self.find_min(nums[1:half+1])

        return self.find_min(nums[half:])


class TestSolution(unittest.TestCase):
    def test_find_min(self):
        s = Solution()

        self.assertEqual(s.find_min([3, 4, 5, 1, 2]), 1)
        self.assertEqual(s.find_min([4, 5, 6, 7, 0, 1, 2]), 0)
        self.assertEqual(s.find_min([0, 1, 2]), 0)
        self.assertEqual(s.find_min([0, 1, 2, 3, 4, 5, 6]), 0)
