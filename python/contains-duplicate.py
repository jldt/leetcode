from typing import List
import unittest


class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        """Given an array of integers, find if the array contains any
        duplicate."""

        s = set()
        for num in nums:
            if num in s:
                return True
            else:
                s.add(num)

        return False


class TestSolution(unittest.TestCase):
    def test_containsDuplicate(self):
        s = Solution()

        self.assertTrue(s.containsDuplicate([1, 1]))
        self.assertFalse(s.containsDuplicate([1, 2, 3, 4]))
        self.assertTrue(s.containsDuplicate([1, 2, 3, 1]))
