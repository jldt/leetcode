import unittest

class Solution:
    def length_longest_substring_no_repetition(self, s: str) -> int:
        """
        Given a string, find the length of the longest substring without
        repeating characters.

        Runtime: faster than 55.67%.
        Memory usage: less than 32.88%.
        """
        if not s:
            return 0

        chars = {}
        start = 0
        max_length = length = 0
        for i, char in enumerate(s):
            if char not in chars:
                chars[char] = i
                length += 1
            else:
                max_length = max(max_length, length)
                start = chars[char] + 1 if chars[char] + 1 > start else start
                chars[char] = i
                length = i - start + 1

        return max(max_length, length)


class TestSolution(unittest.TestCase):
    def test_length_longest(self):
        s = Solution()

        self.assertEqual(s.length_longest_substring_no_repetition("pwwkew"), 3)
        self.assertEqual(
            s.length_longest_substring_no_repetition("abcabcbb"), 3
        )
        self.assertEqual(
            s.length_longest_substring_no_repetition("bépovdljauietsrn"), 16
        )
        self.assertEqual(
            s.length_longest_substring_no_repetition("b"), 1
        )
        self.assertEqual(
            s.length_longest_substring_no_repetition(""), 0
        )
        self.assertEqual(
            s.length_longest_substring_no_repetition("aaaaaaaaaaaaaaaaaaa"), 1
        )
        self.assertEqual(
            s.length_longest_substring_no_repetition("abcccdefgh"), 6
        )
        self.assertEqual(
            s.length_longest_substring_no_repetition("abba"), 2
        )
