from typing import List
import unittest


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        """Given an array `prices` for which the `ith` element is the price of
        a given stock on day `i`.

        You are only permitted to complete at most one transaction (i.e., buy
        one and sell one share of the stock), design an algorithm to find the
        maximum profit.

        Note that you cannot sell a stock before you buy it.
        """
        if len(prices) < 2:
            return 0

        price_buy = prices[0]
        profit = 0

        for price in prices[1:]:
            if price < price_buy:
                price_buy = price
            else:
                tmp = price - price_buy
                if tmp > profit:
                    profit = tmp

        return profit


class TestSolution(unittest.TestCase):
    def test_maxProfit(self):
        s = Solution()

        self.assertEqual(s.maxProfit([]), 0)
        self.assertEqual(s.maxProfit([7, 1, 5, 3, 6, 4]), 5)
        self.assertEqual(s.maxProfit([7, 6, 4, 3, 1]), 0)
