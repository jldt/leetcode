from typing import List
import unittest


class Solution:
    def productExceptSelf(self, nums: List[int]) -> List[int]:
        """Given an array `nums` of n integers where n > 1, return an array
        `output` such that `output[i]` is equal to the product of all the
        elements of `nums` except `nums[i]`.

        It is guaranteed that the product of the elements of any prefix or
        suffis of the array (including the whole array) fits in a 32 bit
        integer.

        Solve it without division and in O(n)."""
        partial_product_ltr = [1] * len(nums)
        partial_product_rtl = [1] * len(nums)

        for i, n in enumerate(nums[:-1]):
            partial_product_ltr[i + 1] = partial_product_ltr[i] * n

        index_last = len(nums) - 1
        for i in range(index_last, 0, -1):
            partial_product_rtl[i - 1] = partial_product_rtl[i] * nums[i]

        output = [1] * len(nums)
        for i in range(0, len(nums)):
            output[i] = partial_product_ltr[i] * partial_product_rtl[i]

        return output


class TestSolution(unittest.TestCase):
    def test_Solution(self):
        s = Solution()

        self.assertEqual(s.productExceptSelf([1, 2, 3, 4]), [24, 12, 8, 6])
        self.assertEqual(s.productExceptSelf([2, 2, 4, 3]), [24, 24, 12, 16])
