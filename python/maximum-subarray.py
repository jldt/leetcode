from typing import List
import unittest


class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        """Given an integer array `nums`, find the contiguous subarray
        (containing at least one number) which has the largest sum and return
        its sum."""
        max_sum = curr_sum = nums[0]
        for n in nums[1:]:
            curr_sum += n
            if n > curr_sum:
                curr_sum = n

            if max_sum < curr_sum:
                max_sum = curr_sum

        return max_sum


class TestSolution(unittest.TestCase):
    def test_maxSubArray(self):
        s = Solution()

        self.assertEqual(s.maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]), 6)
