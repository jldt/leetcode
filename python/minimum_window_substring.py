import unittest
from collections import deque
from math import inf as INF


class Solution:
    def minimum_window_substring(self, s: str, t: str) -> str:
        """
        Given a string S and a string T, find the minimum window in S which
        will contain all the characters in T in complexity O(n).

        Keyword Arguments:
        s: str --
        t: str --
        """
        if not s or not t or len(t) > len(s):
            return ""

        start = end = min_start = 0
        min_length = INF
        chars = {}
        chars_t = {}

        for c in t:
            if c not in chars_t:
                chars_t[c] = 0
                chars[c] = deque()

            chars_t[c] += 1

        has_all_chars = chars_t.copy()

        for i, c in enumerate(s):
            if c in t:
                chars[c].append(i)
                if len(chars[c]) > chars_t[c]:
                    chars[c].popleft()

                if c in has_all_chars:
                    has_all_chars[c] -= 1
                    if not has_all_chars[c]:
                        del has_all_chars[c]

                # Set is empty, we have all the elements.
                if not has_all_chars:
                    min_start = min(min(chars.values()))
                    length = i - min_start + 1
                    if length < min_length:
                        min_length = length
                        start = min_start
                        end = i

            print("-----------")
            print("{}, {}".format(i, c))
            print("chars_t:\t\n{}".format(chars_t))
            print("chars:\t\n{}".format(chars))
            print("min_length: {}".format(min_length))
            print("min_start: {}".format(min_start))
            print("start: {}, end: {}".format(start, end))

        if not has_all_chars:
            return s[start:end+1]

        return ""


class TestSolution(unittest.TestCase):
    def test_minimum_window_substring(self):
        s = Solution()

        # self.assertEqual(
        #     s.minimum_window_substring("ADOBECODEBANC", "ABC"),
        #     "BANC"
        # )
        # self.assertEqual(
        #     s.minimum_window_substring("ADOBECODEBANC", "ABCZ"),
        #     ""
        # )
        # self.assertEqual(
        #     s.minimum_window_substring("ADOBECODEBANC", "AAAA"),
        #     ""
        # )
        self.assertEqual(
            s.minimum_window_substring("ADOBECODEBANC", "AA"),
            "ADOBECODEBA"
        )
        # self.assertEqual(
        #     s.minimum_window_substring("ADOBECODEBANC", ""),
        #     ""
        # )
        # self.assertEqual(
        #     s.minimum_window_substring("", "ABC"),
        #     ""
        # )
        # self.assertEqual(
        #     s.minimum_window_substring("ABBA", "ABBA"),
        #     "ABBA"
        # )
