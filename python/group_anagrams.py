"""Exercise from the Leetcode website."""

import unittest
from typing import List, Dict


class Solution:
    """Solution to the exercise!"""

    def str_to_dict(self, string: str) -> Dict[str, int]:
        """Generate a dict representation of all the characters of `string`."""
        dict_str = {}
        for char in string:
            if char not in dict_str:
                dict_str[char] = 1
            else:
                dict_str[char] += 1

        return dict_str

    # EVALUATION:
    # - Runtime: faster than 5.07%
    # - Memory: less than 15.09%
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        """Given an array of strings, group anagrams together."""
        if not strs:
            return [[]]

        res = []
        d_strs = {}

        for str1 in strs:
            d_str1 = self.str_to_dict(str1)

            no_match = True
            for i, (str2, d_str2) in enumerate(d_strs.items()):
                if len(str1) == len(str2) and d_str1 == d_str2:
                    res[i].append(str1)
                    no_match = False
                    break

            if no_match:
                res.append([str1])
                d_strs[str1] = d_str1

        return res

    # EVALUATION:
    # - Runtime: faster than 87.46%
    # - Memory: less than 94.34%
    def group_anagrams(self, strs: List[str]) -> List[List[str]]:
        """"""
        d_strs = {}
        for string in strs:
            sorted_str = "".join(sorted(string))
            if sorted_str in d_strs:
                d_strs[sorted_str].append(string)
            else:
                d_strs[sorted_str] = [string]

        return list(d_strs.values())


class TestSolution(unittest.TestCase):
    """Unit tests!"""

    def test_group_anagrams(self):
        """Unit test for `group_anagrams` method."""
        sol = Solution()

        self.assertEqual(
            sol.groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]),
            [
                ["eat", "tea", "ate"],
                ["tan", "nat"],
                ["bat"]
            ]
        )
        self.assertEqual(
            sol.groupAnagrams(["eat"]),
            [
                ["eat"],
            ]
        )
        self.assertEqual(
            sol.groupAnagrams([]),
            [
                [],
            ]
        )
        self.assertEqual(
            sol.groupAnagrams(["", ""]),
            [
                ["", ""],
            ]
        )
        self.assertEqual(
            sol.groupAnagrams(["", '']),
            [
                ["", ''],
            ]
        )
