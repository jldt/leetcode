import unittest


class Solution:
    def isValid(self, s: str) -> bool:
        """Given a string containing just the characters `(`, `)`, `{`, `}` and
        `[`, `]`, determine if the input string is valid.

        An input string is valid if:

        1. Open brackets must be closed by the same type of brackets.
        2. Open brackets must be closed in the correct order.

        Note that an empty string is also considered valid."""
        if len(s) == 0:
            return True

        pairs = {'(': ')', '{': '}', '[': ']'}
        heap = []
        for char in s:
            if char in pairs:
                heap.append(pairs[char])
            else:
                if len(heap) == 0 or char != heap.pop():
                    return False

        return len(heap) == 0


class TestSolution(unittest.TestCase):
    def test_isValid(self):
        s = Solution()

        self.assertTrue(s.isValid(""))
        self.assertTrue(s.isValid("()"))
        self.assertTrue(s.isValid("()[]{}"))
        self.assertTrue(s.isValid("([{}])"))
        self.assertFalse(s.isValid("[)"))
        self.assertFalse(s.isValid(")"))
        self.assertTrue(s.isValid("((((()))()))"))
        self.assertFalse(s.isValid("((((())))))"))
