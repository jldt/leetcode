import unittest


class ListNode:
    def __init__(self, val):
        self.val = val
        self.next = None

    def has_cycle(self) -> bool:
        """Return whether or not a linked list has a cycle."""
        hare = turtle = self
        while hare and hare.next:
            turtle = turtle.next
            hare = hare.next.next

            if turtle == hare:
                return True

        return False
