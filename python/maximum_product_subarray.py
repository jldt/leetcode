"""Exercise from the Leetcode website."""

import unittest
from typing import List


class Solution:
    """Solution to the exercise!"""

    def max_product_subarray(self, nums: List[int]) -> int:
        """Given an integer array, find the contiguous subarray (containing at least
        one number) which has the largest product.

        EVALUATION:
        - runtime: faster than 94.36% of submissions;
        - memory: less than 27.59% of submissions.
        """
        if not nums:
            return 0

        if len(nums) == 1:
            return nums[0]

        candidate = nums[0]
        plus = minus = minus_delayed = 0

        for number in nums:

            if number == 0:
                candidate = max(plus, minus, minus_delayed, candidate, number)
                plus = minus = minus_delayed = 0

            elif number < 0:
                if minus:
                    minus *= number
                    if not minus_delayed:
                        minus_delayed = number
                    else:
                        minus_delayed *= number

                else:
                    if plus:
                        minus = number * plus
                    else:
                        minus = number

                candidate = max(plus, minus, minus_delayed, candidate)
                plus = 0

            else:
                if not plus:
                    plus = number
                else:
                    plus *= number

                if minus:
                    minus *= number
                    if minus_delayed:
                        minus_delayed *= number
                    else:
                        minus_delayed = number

                candidate = max(plus, minus, minus_delayed, candidate)

        return candidate


class TestSolution(unittest.TestCase):
    """Unit tests!"""

    def test_max_product_subarray(self):
        """Unit test for `max_product_subarray` method."""
        sol = Solution()

        self.assertEqual(sol.max_product_subarray([2, 3, -2, 4]), 6)
        self.assertEqual(sol.max_product_subarray([-2, -3, -2, -4]), 48)
        self.assertEqual(sol.max_product_subarray([-2, 3, 2, 4]), 24)
        self.assertEqual(sol.max_product_subarray([-2, 0, -1]), 0)
        self.assertEqual(sol.max_product_subarray([-2, -3, 7]), 42)
        self.assertEqual(sol.max_product_subarray([-4, -3, -2]), 12)
        self.assertEqual(sol.max_product_subarray([-2, -3, 7, -1, -2]), 84)
        self.assertEqual(sol.max_product_subarray([2, -5, -2, -4, 3]), 24)
        self.assertEqual(sol.max_product_subarray([1, 0, -1, 2, 3, -5, -2]),
                         60)
        self.assertEqual(sol.max_product_subarray([3, -2, -3, 3, -1, 0, 1]),
                         54)
        self.assertEqual(sol.max_product_subarray([2, -5, -2, -4, 3, -2]),
                         480)
