"""Exercise from the Leetcode website."""

import unittest
from typing import List


class Solution:
    """Solution to the exercise!"""

    def solution_to_exercise(self, bools: List[bool]) -> List[bool]:
        """Solution."""


class TestSolution(unittest.TestCase):
    """Unit tests!"""

    def test_solution_to_exercise(self):
        """Unit test for `solution_to_exercise` method."""
